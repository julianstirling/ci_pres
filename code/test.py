import unittest
from winning.file2 import get_letter_values

class LetterValueTestCase(unittest.TestCase):

    def setUp(self):
        self.values = get_letter_values()

    def test_a(self):
        self.assertEqual(self.values['A'], 1)

    def test_b(self):
        self.assertEqual(self.values['B'], 3)

if __name__ == '__main__':
    testsuite = unittest.TestLoader().discover('.')
    unittest.TextTestRunner().run(testsuite)
