import re
import logging

with open('two_letter.txt','r') as TwoLetterFile,    open('letter_value.txt',   'r')  as value_file: 
    two_letter_words=TwoLetterFile.read()
    for word in two_letter_words.replace(' ','\n').split('\n'):
        if len(word) > 0:
            l1 = word[0]
            l2 = word[1]
            for line in value_file.read().split('\n'):
                try:
                    v1 = int(re.findall(f'{l1} is worth ([0-9]+)', line)[0])
                except:
                    pass
            value_file.seek(0)
            for line in value_file.read().split('\n'):
                try:
                    v2 = int(re.findall(f'{l2} is worth ([0-9]+)', line)[0])
                except:
                    pass
            value_file.seek(0)
            print(f'{word} scores {v1+v2}')


