"""This program might help be beat Joe at Scrabble"""
import re

def main():
    """Prints the values of all two letter words"""
    letter_values = get_letter_values()
    two_letter_words = get_two_letter_words()
    for word in two_letter_words:
        word_value = get_value(word, letter_values)
        print(f'{word} scores {word_value}')

def get_letter_values():
    """Returns the values of each letter in a dictionary"""
    value_dict = {}
    with open('letter_value.txt', 'r')  as value_file:
        value_file_text = value_file.read()
    letter_values = re.findall('([A-Z]) is worth ([0-9]+)', value_file_text)
    for letter, value in letter_values:
        value_dict[letter] = int(value)
    return value_dict

def get_two_letter_words():
    """Returns all two letter words (even the stupid ones)"""
    with open('two_letter.txt', 'r') as two_letter_file:
        two_letter_words = two_letter_file.read()
    two_letter_words = two_letter_words.replace(' ', '\n')
    word_list = two_letter_words.split('\n')
    return [word for word in word_list if len(word) > 0]

def get_value(word, letter_values):
    """Returns the value of the input word"""
    total = 0
    for letter in word:
        total += letter_values[letter]
    return total

if __name__ == "__main__":
    main()
