# Dockerised Linting and Unit Tests on the GitLab CI/CD

----------
# What the hell do those words mean?
----------

Assumed knowledge

[ ] Dockerised
[ ] Linting
[x] and
[ ] Unit Tests
[x] on
[x] the
[x] GitLab
[ ] CI/CD
----------
# Lint/Linting

A little is fine, a lot can be untidy. Let it build up in your tumble drier and it will burn your house down.

![](images/Lint.jpg)

![](images/bobble_off.jpg)

----------
# Code Lint

**Ugly code**

- Lines that are really really long and just go on when there should be some sort of break in the flow but it just goes on and on why doesn’t it stop?
- Inconsistent VARIABLE capitalisation
- Inconsistent              spacing

**Hard to understand code**

- Highly nested statements
- Crazy long functions
- Lack of documentation

**Dangerous code/Code smell**

- Repeating the same code block rather than defining a function
- Unnecessarily complicated code
- Importing things and not using them

**Outright errors**

- Using a variable before it is defined
- Spelling the name of a function wrong
----------
# Code equivalent to the Bobble-Off
- PyLint - Just shouts at you no fixing (warns about code smell!)
- AutoPEP8 - Fixes some things
- Black - aggressive fixes code, can’t customise how it runs
- Flake8 - Similar to PyLint


----------
![](images/Bad.png)

----------
![](images/Good.png)

----------

I have PyLint on always when I code python now.


----------
[ ] Dockerised
[x] Linting
[x] and
[ ] Unit Tests
[x] on
[x] the
[x] GitLab
[ ] CI/CD
----------
# Unit Tests
----------

When you learn to program:

- You write code
- You move code into functions
- You test the functions
- You write more code


----------

When you modify your code how do you check you didn’t break anything?

Do you remember to test every possibility again?

----------

Turns out there are tools to automate this.

![](images/Test.png)



----------

You can even get a report of which lines of code were run. After a teeny bit more code (available after). From a terminal I run:


    coverage run test.py; coverage html
----------
![](images/Coverage.png)

----------
[ ] Dockerised
[x] Linting
[x] and
[x] Unit Tests
[x] on
[x] the
[x] GitLab
[ ] CI/CD
----------
# Docker
----------

Imagine you want to run code tasks on a clean computer with nothing else going on. You can

1. Buy a new computer for each task
2. Set up Virtual machines that emulate hardware, and install OSes on them
3. Use “Containers” ← Docker


----------
# Containers
----------
![](images/OS.png)

----------
![](images/OS_docker.png)

----------
# Docker images

A docker image is the information to needed to “spin-up” a container.

There are lots of OS images:

- Alpine Linux
- Ubunutu
- …

There are also images with software or dev environments pre installed

- Python
- Ruby
- Next Cloud


----------
# Dockerised Linting and Unit Tests on the GitLab CI/CD
----------
# CI/CD = Continuous Integration/Continuous Delivery

Continuous integration fancy term for everyone merging into master regularly.

For this to work everyone needs to be compile and testing the merged result.

CI came to mean the the process of automating building and testing code.

----------
# GitLab CI/CD

GitLab can be configured to run code whenever there is a …

- Push
- Merge request
- Tag
- etc

It will run it in a Docker container.

It will run in the cloud or on your own hardware.

----------
# SetUp


## 1 Make a a config file

Put a file called

    .gitlab-ci.yml

in the repository


## 2 Add some rules to the file

E.g.

    image: ubuntu:18.04
    
    
    before_script:
      - apt-get update -qq
      - apt-get -y -qq install openssl python3-pip
      - python3 -m "pip" install pyyaml
    
    build:
      stage: deploy
      script:
      - python3 HardwareScrape.py
      artifacts:
        name: "${CI_PROJECT_NAME}-${CI_JOB_NAME}"
        paths:
        - Output
      only:
      - master

There are lots of instructions, but I tend to just look at other files in repositories and adapt them. There is a tool to check the syntax:
 `https://gitlab.com/{group}/{project}/-/ci/lint`

## 3 Push your repository up to GitLab
![](images/PipelineStatus.png)

![](images/Statuses.png)



----------
![](images/Pipeline.png)



https://gitlab.com/gitbuilding/gitbuilding/-/pipelines


